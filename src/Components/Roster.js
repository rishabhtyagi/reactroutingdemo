import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom'
import FullRoster from './FullRoster'
import Player from './Player'


const Roster = () => {
  let {url} = useRouteMatch();
return (
    <Switch>
    <Route exact path={`${url}`} component={FullRoster}/>
    <Route path={`${url}/:number`} component={Player}/>
  </Switch>
)
}
export default Roster;