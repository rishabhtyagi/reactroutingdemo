import React from 'react'

const Schedule = () => (
  <div>
    <ul>
      <li>India vs South Africa, 14th Match</li>
      <li>India vs TBC, 23rd Match</li>
      <li>India vs England, 29th Match</li>
    </ul>
  </div>
)

export default Schedule
