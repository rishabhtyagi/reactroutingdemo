import React from 'react'
import PlayerAPI from '../api'
import { Link,  useParams, Redirect, Switch} from 'react-router-dom'

// The Player looks up the player using the number parsed from
// the URL's pathname. If no player is found with the given
// number, then a "player not found" message is displayed
const Player = () => {
    let { number } = useParams();
    const player = PlayerAPI.get(
    parseInt(number, 10)
  )
  if (!player) {
    return (
        <Switch>
         <Redirect to='/home'/>
        </Switch>
    )
  }
  return (
    <div>
      <h1>{player.name} (#{player.number})</h1>
      <p>{player.description}</p>
      <Link to='/roster'>Back</Link>
    </div>
  )
}

export default Player
