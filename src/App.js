import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './Components/Header';
import Main from './Components/Main';
import { Switch, Route, Redirect } from 'react-router-dom';

function App() {
  return (
    <div>
      <h1 style={{textAlign: "center"}}>Indian Cricket Team Roster</h1>
      <Header />
      <Main />
    </div>
  );
}

export default App;
